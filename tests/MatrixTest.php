<?php

use PHPUnit\Framework\TestCase;
use VVPS\Matrix;

class MatrixTest extends TestCase{

    public function testValidationValid(){
        $ref = new ReflectionClass(Matrix::class);
        $obj = $ref->newInstanceWithoutConstructor();
        $method = new ReflectionMethod(Matrix::class,'validateData');
        $method->setAccessible(true);
        $data = [
            [1,1],
            [1,1]
        ];
        $res = $method->invoke($obj,$data);
        $this->assertTrue($res);
    }

    public function testValidationNumberException(){
        $this->expectException(Exception::class);
        $this->expectExceptionMessageRegExp('/Matrix\ cell\ \[\d+\,\d+\] must be numeric\./');
        $ref = new ReflectionClass(Matrix::class);
        $obj = $ref->newInstanceWithoutConstructor();
        $method = new ReflectionMethod(Matrix::class,'validateData');
        $method->setAccessible(true);
        $data = [
            [1,1],
            ['f',1]
        ];

        $method->invoke($obj,$data);
    }

    public function testValidationDimensionsException(){
        $this->expectException(Exception::class);
        $this->expectExceptionMessageRegExp('/Matrix\ row\ \[\d+\] must have consistent length\./');
        $ref = new ReflectionClass(Matrix::class);
        $obj = $ref->newInstanceWithoutConstructor();
        $method = new ReflectionMethod(Matrix::class,'validateData');
        $method->setAccessible(true);
        $data = [
            [1,1],
            [1,1,1]
        ];

        $method->invoke($obj,$data);
    }

    public function testValidationNotEmptyException(){
        $this->expectException(Exception::class);
        $this->expectExceptionMessageRegExp('/Matrix\ row\ \[\d+\] can not be empty\./');
        $ref = new ReflectionClass(Matrix::class);
        $obj = $ref->newInstanceWithoutConstructor();
        $method = new ReflectionMethod(Matrix::class,'validateData');
        $method->setAccessible(true);
        $data = [
            [1,1],
            []
        ];

        $method->invoke($obj,$data);
    }

    public function testSwap(){
        $ref = new ReflectionClass(Matrix::class);
        $data = [
            [1,1],
            [2,2]
        ];
        $method = $ref->getMethod('swapRows');
        $prop = $ref->getProperty('data');
        $prop->setAccessible(true);

        $obj = $ref->newInstanceWithoutConstructor();
        $prop->setValue($obj,$data);
        $method->invoke($obj,0,1);
        $expected = [
            [2,2],
            [1,1]
        ];
        $this->assertEquals($expected,$prop->getValue($obj));
    }

    public function testRow(){
        $ref = new ReflectionClass(Matrix::class);
        $data = [
            [1,1],
            [2,2]
        ];
        $method = $ref->getMethod('row');
        $prop = $ref->getProperty('data');
        $prop->setAccessible(true);

        $obj = $ref->newInstanceWithoutConstructor();
        $prop->setValue($obj,$data);
        $res = $method->invoke($obj,0);
        $this->assertEquals([1,1],$res);
    }

    public function testMultiplyRow(){
        $ref = new ReflectionClass(Matrix::class);
        $data = [
            [1,1],
            [2,2]
        ];
        $method = $ref->getMethod('multiplyRow');
        $prop = $ref->getProperty('data');
        $prop->setAccessible(true);

        $obj = $ref->newInstanceWithoutConstructor();
        $prop->setValue($obj,$data);
        $method->invokeArgs($obj,[0,3]);
        $this->assertEquals([3,3],$prop->getValue($obj)[0]);
    }

    public function testMaxIndex(){
        $ref = new ReflectionClass(Matrix::class);
        $data = [
            [1,2],
            [3,4]
        ];
        $method = $ref->getMethod('getMaxIndex');
        $prop = $ref->getProperty('data');
        $prop->setAccessible(true);

        $obj = $ref->newInstanceWithoutConstructor();
        $prop->setValue($obj,$data);
        $res = $method->invokeArgs($obj,[0,0]);
        $this->assertEquals(0,$res);
        $res = $method->invokeArgs($obj,[1,0]);
        $this->assertEquals(0,$res);
        $res = $method->invokeArgs($obj,[0,1]);
        $this->assertEquals(1,$res);
        $res = $method->invokeArgs($obj,[1,1]);
        $this->assertEquals(1,$res);
    }

    public function testMultiplyAndAddToRow(){
        $ref = new ReflectionClass(Matrix::class);
        $data = [
            [1,1],
            [2,2]
        ];
        $method = $ref->getMethod('multiplyAndAddToRow');
        $prop = $ref->getProperty('data');
        $prop->setAccessible(true);

        $obj = $ref->newInstanceWithoutConstructor();
        $obj->dimension = [2,2];
        $prop->setValue($obj,$data);
        $res = $method->invokeArgs($obj,[0,10,1]);
        $this->assertEquals([12,12],$prop->getValue($obj)[1]);
    }
}