<?php
require_once('vendor/autoload.php');

/**
 * To run tests run vendor/phpunit/phpunit/phpunit from root directory
 */
$matrix = [
    [345, 65, 23, 31.4],
    [168, 18, 18, 14.6],
    [94 ,  0,  0, 6.4],
    [187, 185, 98, 28.3],
    [621, 87, 10, 42.1],
    [255, 0 , 0, 15.3]
];
$m = new \VVPS\Matrix($matrix);
if($m->error){
    echo $m->error.PHP_EOL;
}else{
    $m->solve();
    $m->show();
    var_dump($m->triangularize());
}

