<?php
/**
 * @author Donald Ovcharov
 * @version 1.0
 *
 */
namespace VVPS;

use Exception;

/**
 * Class Matrix
 * Solves system of equations using multiple regressions
 * @package VVPS
 */
class Matrix
{

    /**
     * Holds matrix data
     * @var array
     */
    protected $data;
    /**
     * Contains descriptive error
     * @var string
     */
    public $error;

    /**
     * Contains matrix dimensions e.g. [2,3]
     * @var array
     */
    public $dimension;

    /**
     * @param array $data
     */
    public function __construct(array $data)
    {

        try {
            $this->validateData($data);
            $this->data = $data;
            $this->processData();
            $this->extractDimension();
        } catch (Exception $e) {
            $this->error = $e->getMessage();
        }
    }

    /**
     * Solves processed matrix resulting in a normal matrix
     */
    public function solve()
    {
        for ($i = 0; $i < $this->dimension[0]; $i++) {
            $max_row_index = $this->getMaxIndex($i, $i);
            $this->swapRows($i, $max_row_index);

            $this->multiplyRow($i, 1 / $this->data[$i][$i]);
            for ($k = $i + 1; $k < $this->dimension[0]; $k++) {
                $this->multiplyAndAddToRow($i, -1 * $this->data[$k][$i], $k);
            }
        }
    }

    /**
     * Uses nprmalized matrix to solve for each variable
     */
    public function triangularize()
    {
        $b4 = $this->data[3][4];
        $b3 = $this->data[2][4] - $this->data[2][3] * $b4;
        $b2 = $this->data[1][4] - $this->data[1][3] * $b4 - $this->data[1][2] * $b3;
        $b1 = $this->data[0][4] - $this->data[0][3] * $b4 - $this->data[0][2] * $b3 - $this->data[0][1] * $b2;
        return [$b1, $b2, $b3, $b4];
    }

    /**
     * Displays the matrix in a human readable format
     */
    public function show()
    {
        array_walk($this->data, function ($row) {
            echo implode(',', $row);
            echo PHP_EOL;
        });
    }

    /**
     * Setter and getter for a matrix row
     * @param $i
     * @param bool|false $row
     * @return mixed
     */
    public function row($i, $row = false)
    {
        if ($row) {
            $this->data[$i] = $row;
        }
        return $this->data[$i];
    }

    /**
     * Finds the maximum index in a given column starting from a given row
     * @param $col
     * @param int $start
     * @return int
     */
    public function getMaxIndex($col, $start = 0)
    {
        $max_val = $this->data[$start][$col];
        $max_index = $start;
        for ($i = $start; $i < $this->dimension[0]; $i++) {
            $row = $this->data[$i];
            $el = $row[$col];
            if ($max_val < $el) {
                $max_val = $el;
                $max_index = $i;
            }
        }
        return $max_index;
    }

    /**
     * Multiplies a row by a coefficient
     * @param $i
     * @param $coef
     */
    public function multiplyRow($i, $coef)
    {
        $this->row($i, $this->getMultipliedRow($i, $coef));
    }

    /**
     * Gets a multiplied row by a given coefficient
     * @param $i
     * @param $coef
     * @return array|mixed
     */
    public function getMultipliedRow($i, $coef)
    {
        $row = $this->row($i);
        $row = array_map(function ($el) use ($coef) {
            return $el * $coef;
        }, $row);
        return $row;
    }

    /**
     * Multiplies a row and adds it to anothe row.
     * This is a common operation when working with matrixes
     * @param $i
     * @param $coef
     * @param $target
     */
    public function multiplyAndAddToRow($i, $coef, $target)
    {
        $addition = $this->getMultipliedRow($i, $coef);
        $row = $this->row($target);
        for ($i = 0; $i < $this->dimension[1]; $i++) {
            $row[$i] += $addition[$i];
        }
        $this->row($target, $row);
        return $this->row($target);
    }

    /**
     * Swaps two rows inside the matrix
     * This is a common operation when working with matrixes
     * @param $a
     * @param $b
     */
    public function swapRows($a, $b)
    {
        $tmp = $this->row($a);
        $this->row($a, $this->row($b));
        $this->row($b, $tmp);
    }

    /**
     * Uses the matrix data to determine the matrix dimensions
     */
    protected function extractDimension()
    {
        $Ny = count($this->data);
        $Nx = count($this->data[0]);
        $this->dimension = [$Ny, $Nx];
    }

    /**
     * Converts the matrix data to a multiple regression matrix
     */
    public function processData()
    {
        $data = [];
        //row1
        $row = [];
        $row[0] = count($this->data);
        for ($i = 0; $i < count($this->data[0]); $i++) {
            $row[$i + 1] = $this->sumCol(function ($row) use ($i) {
                return $row[$i];
            });
        }
        $data[] = $row;

        //row2
        $row = [];
        $row[0] = $this->sumCol(function ($row) {
            return $row[0];
        });
        for ($i = 0; $i < count($this->data[0]); $i++) {
            $row[$i + 1] = $this->sumCol(function ($row) use ($i) {
                return $row[0] * $row[$i];
            });
        }
        $data[] = $row;

        //row3
        $row = [];
        $row[0] = $this->sumCol(function ($row) {
            return $row[1];
        });
        for ($i = 0; $i < count($this->data[0]); $i++) {
            $row[$i + 1] = $this->sumCol(function ($row) use ($i) {
                return $row[1] * $row[$i];
            });
        }
        $data[] = $row;

        //row4
        $row = [];
        $row[0] = $this->sumCol(function ($row) {
            return $row[2];
        });
        for ($i = 0; $i < count($this->data[0]); $i++) {
            $row[$i + 1] = $this->sumCol(function ($row) use ($i) {
                return $row[2] * $row[$i];
            });
        }
        $data[] = $row;
        $this->data = $data;
    }

    /**
     * Returns the sum of all column values passed through a given function
     * @param $callback
     * @return number
     */
    public function sumCol($callback)
    {
        return array_sum(array_map($callback, $this->data));
    }

    /**
     * Verifies the input matrix is valid
     * @param array $data
     * @throws Exception
     */
    protected function validateData(array $data)
    {
        //Validate array is not empty
        if (empty($data)) {
            throw new Exception("Matrix can not be empty.");
        }
        //Validate 1st element is array
        if (!is_array($data[0])) {
            throw new Exception("Matrix row [1] must be array.");
        }
        //Validate 1st element is not empty
        if (empty($data[0])) {
            throw new Exception("Matrix row [1] can not be empty.");
        }

        $Ny = count($data);
        $Nx = count($data[0]);
        for ($i = 0; $i < $Ny; $i++) {
            //Validate all other elements are not empty arrays with consistant array length
            if (!is_array($data[$i])) {
                throw new Exception("Matrix row [" . ($i + 1) . "] must be array.");
            }
            if (empty($data[$i])) {
                throw new Exception("Matrix row [" . ($i + 1) . "] can not be empty.");
            }
            if (count($data[$i]) != $Nx) {
                throw new Exception("Matrix row [" . ($i + 1) . "] must have consistent length.");
            }

            for ($k = 0; $k < $Nx; $k++) {
                if (!is_numeric($data[$i][$k])) {
                    throw new Exception("Matrix cell [" . ($i + 1) . "," . ($k + 1) . "] must be numeric.");
                }
            }
        }
        return true;
    }
}